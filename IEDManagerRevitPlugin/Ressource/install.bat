@echo off
(
@echo ^<?xml version="1.0" encoding="utf-8" standalone="no"?^>
@echo ^<RevitAddIns^>
@echo ^<AddIn Type="Command"^>
@echo ^<Assembly^>%~dp0\..\IEDManagerRevitPlugin.dll^</Assembly^>
@echo ^<ClientId^>502fe383-2668-4e98-aff8-5e6047f9dc34^</ClientId^>
@echo ^<FullClassName^>IEDManagerPlugin^</FullClassName^>
@echo ^<Text^>Export IED^</Text^>
@echo ^<VendorId^>MathieuDupuis^</VendorId^>
@echo ^<VisibilityMode^>AlwaysVisible^</VisibilityMode^>
@echo ^</AddIn^>
@echo ^</RevitAddIns^>
) > "%appData%\Autodesk\Revit\Addins\2017\IEDManagerPlugin.addin"
set /p UserInputPath= Instalation complet, press enter to close this window
@echo on