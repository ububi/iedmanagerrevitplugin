﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.Attributes;
using org.ububiGroup.IEDManager.IO.generic;
using org.ububiGroup.IEDManager.IO;
using org.ububiGroup.IEDManager.model.BIM;


[TransactionAttribute(TransactionMode.Manual)]
[RegenerationAttribute(RegenerationOption.Manual)]
public class IEDManagerPlugin : IExternalCommand
{
    #region Private member
    private UIApplication uiApp;
    private Document doc;

    private SaveFileDialog saveDlg;

    ExportOption[] lstExportOptions;
    List<ElementId> lstType;
    List<char> acceptedUniformatCode;
    List<CompositeKey<ElementId, ElementId>> lstMaterialExported;
    baseExporter BIMExp;

    #endregion

    public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
    {
        uiApp = commandData.Application;
        doc = uiApp.ActiveUIDocument.Document;

        lstMaterialExported = new List<CompositeKey<ElementId, ElementId>>();
        lstType = new List<ElementId>();
        lstType.Add(null);
        lstType.Add(ElementId.InvalidElementId);

        acceptedUniformatCode = new List<char>();
        acceptedUniformatCode.AddRange(new char[] { 'A', 'B', 'C', 'G' });

        lstExportOptions = ExportOptionManager.getExportOptionManager().getExportOptionList();

        setupDlg();
        int iFileType = -1;
        String outputFile = getOutputFile(ref iFileType);
        if (!String.IsNullOrEmpty(outputFile))
        {
            BIMExp = lstExportOptions[iFileType-1].getExporter();
            BIMExp.init(outputFile);
            ExportObjects(); //Export objects, type and material at the same time
            BIMExp.close();
        }

        return Result.Succeeded;
    }

    private String getOutputFile(ref int fileType)
    {
        if (saveDlg.ShowDialog() == DialogResult.OK)
        {
            if (isValidFilePath(saveDlg.FileName))
            {
                fileType = saveDlg.FilterIndex;
                return saveDlg.FileName;
            }
        }
        return "";
    }

    private bool isValidFilePath(String FilePath)
    {
        try
        {
            FileInfo fi = new FileInfo(FilePath);
            return fi != null;
        }
        catch
        {
            return false;
        }
    }

    private void setupDlg()
    {
        saveDlg = new SaveFileDialog();
        String filter = "";
        int i = 1;
        int iFilter = 1;
        foreach (ExportOption eo in lstExportOptions)
        {
            if (!String.IsNullOrEmpty(filter))
                filter += "|";
            filter += eo.getName() + " file|*." + eo.getExtension();

            if (eo.getName() == "IED")
                iFilter = i;

            i++;
        }
        saveDlg.Filter = filter;
        saveDlg.FilterIndex = iFilter;
    }

    #region Model exporter

    public void ExportObjects()
    {
        
        FilteredElementCollector collector = new FilteredElementCollector(doc).WhereElementIsNotElementType();
        foreach (Element e in collector)
        {
            if (e.Category != null && e.Category.HasMaterialQuantities)
            {                
                double Area;
                double Volume;
                String TypeUCode;
                String ElemUCode = GetUniformatCode(e);

                saveType(e, out TypeUCode);

                if (exportNeeded(TypeUCode) && exportNeeded(ElemUCode))
                {
                    
                    CalculateObjectQty(e, out Area, out Volume); //Export material too

                    BIMExp.ExportBIMObject(new BIMObject(e.Id.IntegerValue,
                                                        e.Name,
                                                        e.GetTypeId().IntegerValue,
                                                        e.GetType().ToString(),
                                                        e.Category.Name,
                                                        (String.IsNullOrWhiteSpace(ElemUCode) ? TypeUCode : ElemUCode),
                                                        Area,
                                                        Volume));
                }
            }
        }

    }

    private void saveType(Element e,out String TypeUCode)
    {
        TypeUCode = "";
        ElementId TId = e.GetTypeId();

        ElementType EType = doc.GetElement(TId) as ElementType;

        TypeUCode = GetUniformatCode(EType);

        if (!lstType.Contains(EType.Id))
        {
            if (exportNeeded(TypeUCode))
            {
                BIMExp.ExportBIMObjectType(new BIMObjectType(EType.Id.IntegerValue,
                                                            EType.Name,
                                                            TypeUCode,
                                                            GetUniformatDescription(EType),
                                                            EType.FamilyName));
            }
            lstType.Add(EType.Id);
        }
    }

    private void CalculateObjectQty(Element e, out Double area, out Double volume)
    {
        //TODO Process paint element

        Double precision = 0.00000001; //Double are not exact so we need a buffer to validate ex: 0.1 --> 0.10000000000000001
        Double maxArea = 0;
        Double max2area = 0;
        volume = 0;

        int idOwner = e.GetTypeId().IntegerValue;

        List<BIMMaterial> lstMaterial = new List<BIMMaterial>();

        //find the object Area and volume
        foreach (ElementId matId in e.GetMaterialIds(false))
        {
            Material mat = (Material)doc.GetElement(matId);
            
            Double materialArea = UnitUtils.ConvertFromInternalUnits(e.GetMaterialArea(matId, false),DisplayUnitType.DUT_SQUARE_METERS);
            Double materialVolume = UnitUtils.ConvertFromInternalUnits(e.GetMaterialVolume(matId), DisplayUnitType.DUT_CUBIC_METERS);

            CompositeKey<ElementId, ElementId> key = new CompositeKey<ElementId, ElementId>(matId,e.GetTypeId());
            if (!lstMaterialExported.Contains(key))
            {
                lstMaterial.Add(new BIMMaterial(key.key1.IntegerValue,
                                                        key.key2.IntegerValue,
                                                        mat.Name,
                                                        materialArea,
                                                        materialVolume, 0, 0));
                lstMaterialExported.Add(key);
            }

            if (materialArea-maxArea > precision)
            {
                max2area = maxArea;
                maxArea = materialArea;
            }
            
            volume += materialVolume;
        }

        //When a wall have the same finish on two side the Area is double. So we need to take the area for only one side.
        if (Math.Abs(maxArea - (max2area * 2)) < precision)
        {
            area = max2area;
        }
        else
        {
            area = maxArea;
        }

        //Save material with area and volume ratio
        foreach (BIMMaterial material in lstMaterial)
        {
            if (Math.Abs(area) > precision)
                material.setRatioArea(material.getArea() / area);

            if (Math.Abs(volume) > precision)
                material.setRatioVolume(material.getVolume() / volume);

            BIMExp.ExportBIMMaterial(material);
        }

    }
    
    #endregion

    #region Utility

    private String GetUniformatCode(Element e)
    {
        String code = GetParameter(e, BuiltInParameter.UNIFORMAT_CODE);

        return code;
    }

    private String GetUniformatDescription(Element e)
    {
        String Desc = GetParameter(e, BuiltInParameter.UNIFORMAT_DESCRIPTION);

        return Desc;
    }

    private String GetParameter(Element e, String parameterId)
    {
        if (e.Parameters != null)
        {
            IList<Parameter> LstParam = e.GetParameters(parameterId);
            
            if (LstParam != null)
            {
                String ParamValue = "";
                foreach(Parameter Param in LstParam)
                {
                    String Tmp = Param.AsString();
                    if (!String.IsNullOrWhiteSpace(Tmp))
                        ParamValue = Tmp;
                }
                return ParamValue;
            }
        }
        return "";
    }

    private String GetParameter(Element e, BuiltInParameter parameterId)
    {
        if (e.Parameters != null)
        {
            Parameter param = e.get_Parameter(parameterId);
            if (param != null)
            {
                String paramValue = param.AsString();
                if (!String.IsNullOrEmpty(paramValue))
                    return paramValue;
            }
        }
        return "";
    }

    private Boolean exportNeeded(String uniformatCode)
    {
        if (String.IsNullOrWhiteSpace(uniformatCode))
            return true;

        return acceptedUniformatCode.Contains(uniformatCode.ToUpper()[0]);
    }

    #endregion
    
}