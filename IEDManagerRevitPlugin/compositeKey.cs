﻿using System;

public class CompositeKey<U, V>
{
    public U key1 { get; set; }
    public V key2 { get; set; }

    public CompositeKey(U key1, V key2)
    {
        this.key1 = key1;
        this.key2 = key2;
    }

    public override bool Equals(object obj)
    {
        CompositeKey<U, V> tmp = obj as CompositeKey<U, V>;
        if (tmp == null)
            return false;
        
        return keyEquals(this.key1,tmp.key1) && keyEquals(this.key2, tmp.key2);
    }

    // Source: http://stackoverflow.com/questions/18065251/concise-way-to-combine-field-hashcodes
    public override int GetHashCode()
    {
        unchecked // Overflow is fine, just wrap
        {
            int hash = 17;
            
            hash = hash * 23 + (key1 != null ? key1.GetHashCode() : 0);
            hash = hash * 23 + (key2 != null ? key2.GetHashCode() : 0);
            return hash;
        }
    }

    private static bool keyEquals(object obj1, object obj2)
    {
        if (obj1 == null)
            return obj1 == null;
        else
            return obj1.Equals(obj2);
    }
}
