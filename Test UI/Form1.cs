﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test_UI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String ObjPath=txtObj.Text;
            String TypePath=txtType.Text;
            String MaterialPath = txtMaterial.Text;

            IEDManagerRevitPlugin.FrmExportDlg.GetIEDPath(ref ObjPath,ref TypePath, ref MaterialPath);

            txtObj.Text = ObjPath;
            txtType.Text = TypePath;
            txtMaterial.Text = MaterialPath;
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
